const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");
const { google } = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const User = mongoose.model('User');
const fs = require('fs');
const ctrlServer = require('../controllers/server.controller');

const starbound_config = '/home/steam/starbound_server/storage/starbound_server.config';
// const starbound_config = 'E:\\Dev\\WalkaStarbound\\server\\starbound_server.config'

const oauth2Client = new OAuth2(
    process.env.SB_CLIENT_ID,
    process.env.SB_CLIENT_SECRET,
    "https://developers.google.com/oauthplayground"
);
oauth2Client.setCredentials({
    refresh_token: process.env.SB_REFRESH_TOKEN
});

const accessToken = oauth2Client.getAccessToken()
const smtpTransporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        type: "OAuth2",
        user: process.env.SB_USER,
        clientId: process.env.SB_CLIENT_ID,
        clientSecret: process.env.SB_CLIENT_SECRET,
        refreshToken: process.env.SB_REFRESH_TOKEN,
        accessToken: accessToken
    }
})

function writeStarboundAccount(user) {
    let rawdata = fs.readFileSync(starbound_config);
    let starConfig = JSON.parse(rawdata);
    starConfig.serverUsers[user.pseudo] = {
        admin: false,
        password: user.starboundPassword
    };
    let data = JSON.stringify(starConfig, undefined, 2);
    fs.writeFileSync(starbound_config, data);
    ctrlServer.reloadServer();
}

module.exports.register = (req, res, next) => {
    var user = new User();
    user.pseudo = req.body.pseudo;
    user.email = req.body.email;
    user.password = req.body.password;
    user.starboundPassword = req.body.starboundPassword;
    user.save((err, doc) => {
        if (!err) {
            res.send(doc);
            sendMail(user);
            writeStarboundAccount(user);
        } else {
            if (err.code === 11000)
                if (err.errmsg.includes(user.email)) {
                    res.status(422).send(['Adresse e-mail déjà utilisé.']);
                } else if (err.errmsg.includes(user.pseudo)) {
                    res.status(422).send(['Pseudo déjà utilisé.']);
                } else {
                    return next(err);
                }
        }
    });
};

module.exports.updateProfile = (req, res, next) => {
    try {
        jwt.verify(req.body.token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return res.status(500).send({status: false, message: 'Mauvais Token'});
            } else {
                User.findOne({_id: decoded._id}, (err, user) => {
                    if (!user)
                        return res.status(404).json({status: false, message: 'Utilisateur non trouvé'});
                    else {
                        let reloadServer = false;
                        let response = {
                            message: '',
                            error: ''
                        };
                        if (req.body.email && req.body.email !== user.email) {
                            user.email = req.body.email;
                            if (!user.compteActive) {
                                response.error += 'E-Mail non changé<br>Veuiller d\'abord valider la nouvelle<br><br>'
                            } else {
                                user.compteActive = false;
                                sendMail(user);
                                response.message += 'E-Mail mis à jour<br>Veuiller la valider.<br><br>';
                            }
                        }
                        if (req.body.password && !user.verifyPassword(req.body.password)) {
                            user.password = req.body.password;
                            user.passwordModified = true;
                            response.message += 'Mot de Passe mis à jour<br><br>';
                        }
                        if (req.body.starboundPassword) {
                            let d1 = new Date();
                            let d2 = user.lastStarboundPasswordModification;
                            d1.setHours(0, 0, 0, 0);
                            d2.setHours(0, 0, 0, 0);
                            let diff = Math.abs((d1.getTime() - d2.getTime())) / (1000 * 60 * 60 * 24);
                            if (diff > 31) {
                                user.starboundPassword = req.body.starboundPassword;
                                user.lastStarboundPasswordModification = d1;
                                user.markModified('lastStarboundPasswordModification');
                                response.message += 'Mot de Passe Starbound mis à jour<br><br>';
                                reloadServer = true;
                            } else {
                                let err = 'Vous avez mis à jour votre mot de passe Starbound<br>';
                                if (diff === 0) {
                                    err += 'aujourd\'hui<br>';
                                } else {
                                    err += 'il y a: ' + diff.toString() + ' jours<br>';
                                }
                                err += 'Vous pourrez changer votre mot de passe dans:<br>';
                                err += (31 - diff).toString() + 'jours<br><br>';
                                response.error += err
                            }
                        }
                        user.save();
                        if (!response.message && !response.error) {
                            response.message = 'Rien a changé ...'
                        } else if (response.message) {
                            response.message += `Profile mis à jour ${user.pseudo}`;
                        }
                        if (reloadServer) {
                            try {
                                writeStarboundAccount(user);
                            } catch (e) {
                                response.error += e.toString()
                            }
                        }
                        return res.status(200).json(response);
                    }
                });
            }
        });
    } catch (e) {
        res.send(JSON.stringify(e))
    }
};

module.exports.authenticate = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            // error from passport middleware
            console.log("400 :" + JSON.stringify(err));
            return res.status(400).json(err);
        } else if (user) {
            // registered user
            if (user.compteActive) {
                return res.status(200).json({"token": user.generateJwt()});
            } else {
                return res.status(200).json({"active": false})
            }
        } else {
            // unknown user or wrong password
            console.log("404: " + JSON.stringify(info));
            return res.status(404).json(info);
        }
    })(req, res);
};

module.exports.confirmation = (req, res, next) => {
    try {
        jwt.verify(req.params.token, process.env.JWT_SECRET,
            (err, decoded) => {
                if (err)
                    return res.status(500).send({status: false, message: 'Mauvais Token'});
                else {
                    User.findOne({_id: decoded._id}, (err, user) => {
                        if (!user)
                            return res.status(404).json({status: false, message: 'Utilisateur non trouvé'});
                        else {
                            user.compteActive = true;
                            user.save();
                            return res.status(200).json({status: true, message: `Compte activé ${user.pseudo}`});
                        }
                    });
                }
            });
    } catch (e) {
        res.send(JSON.stringify(e))
    }
};

module.exports.userProfile = (req, res, next) => {
    User.findOne({_id: req._id}, (err, user) => {
            if (!user)
                return res.status(404).json({status: false, message: 'Utilisateur non trouvé'});
            else
                return res.status(200).json({status: true, user: _.pick(user, ['pseudo', 'email', 'starboundPassword'])});
        }
    );
};

async function sendMail(user) {
    // let testAccount = await nodemailer.createTestAccount();
    let token = user.generateJwt();
    let confirm_url = `http://walka-starbound.servegame.com/auth/confirmation/${token}`;
    let html = `Confirmé votre compte en <a href="${confirm_url}">cliquant ici</a><br><br>Ou copié cette url dans votre navigateur: ${confirm_url}`;
    let info = await smtpTransporter.sendMail({
        from: '"Starbound WALKA 👻" <starbound.walka@gmail.com>', // sender address
        to: user.email, // list of receivers
        subject: "Confirmation du compte WALKA Starbound ✔", // Subject line
        text: `Confirmer votre mail en copiant cette url dans votre navigateur: ${confirm_url}`, // plain text body
        html: html // html body
    });
    // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    return info;
}
