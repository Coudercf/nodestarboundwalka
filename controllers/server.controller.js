const {spawn} = require('child_process');


module.exports.getNbPlayer = (req, res, next) => {
    if (!process.env.NODE_ENV) {
        return res.status(200).json({nb_player: 0})
    } else {
        const netstat = spawn('sh', ['-c', 'netstat -na | grep ESTABLISHED | grep 21025 |wc -l']);
        netstat.stdout.on('data', (data) => {
            console.log('Player: ', data.toString());
            return res.status(200).json({nb_player: data.toString()})
        })
    }
};

module.exports.isOnline = (req, res, next) => {
    if (!process.env.NODE_ENV) {
        return res.status(200).json({status: false})
    } else {
        const netstat = spawn('sh', ['-c', 'netstat -na | grep LISTEN | grep 21025 | wc -l']);
        netstat.stdout.on('data', (data) => {
            let status = parseInt(data.toString());
            if (status >= 1)
                return res.status(200).json({status: true});
            else
                return res.status(200).json({status: false})
        })
    }
};

module.exports.reloadServer = () => {
    if (!process.env.NODE_ENV) {
        console.log("rcon command")
    } else {
        spawn('rcon', ['-H', '0.0.0.0', '-p', '21026', '-P', 'florient69', 'serverreload'])
    }
};
