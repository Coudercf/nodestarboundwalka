const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');

var User = mongoose.model('User');

passport.use(
    new LocalStrategy({usernameField: 'email'}, (username, password, done) => {
        User.findOne({email: username}, (err, user) => {
            if (err)
                return done(err);
            else if (!user) // unknown user
                return done(null, false, {message: 'Cette e-mail n\'existe pas'});
            else if (!user.verifyPassword(password)) // wrong password
                return done(null, false, {message: 'Mauvais mot de passe'});
            else // authentication succeeded
                return done(null, user);
        });
    })
);
