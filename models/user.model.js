const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
    pseudo: {
        type: String,
        required: 'Pseudo requis',
        unique: true
    },
    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },
    password: {
        type: String,
        required: 'Mot de Passe requis !',
        minlength: [6, 'Mot de passe doit contenir au moins 6 charactère']
    },
    starboundPassword: {
        type: String,
        required: 'Mot de Passe à utiliser sur Starbound requis',
        minlength: [6, 'Mot de passe doit contenir au moins 6 charactère']
    },
    lastStarboundPasswordModification: {
        type: Date,
        default: Date.now
    },
    compteActive: {
        type: Boolean,
        default: false
    },
    passwordModified: {
        type: Boolean,
        default: true
    },
    saltSecret: {
        type: String,
        default: ''
    }
});

// Custom validation for email
userSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'E-Mail invalide.');

// Events
userSchema.pre('save', function (next) {
    if (this.passwordModified) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(this.password, salt, (err, hash) => {
                this.password = hash;
                this.saltSecret = salt;
                this.passwordModified = false;
                next();
            });
        });
    } else {
        next();
    }
});

// Methods
userSchema.methods.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.generateJwt = function () {
    return jwt.sign(
        {_id: this._id},
        process.env.JWT_SECRET,
        {expiresIn: process.env.JWT_EXP}
    );
};


mongoose.model('User', userSchema);
