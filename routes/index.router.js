const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const ctrlServer = require('../controllers/server.controller');
const jwtHelper = require('../config/jwtHelper');

router.post('/register', ctrlUser.register);
router.post('/update-profile', ctrlUser.updateProfile);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/confirmation/:token', ctrlUser.confirmation);
router.get('/userProfile', jwtHelper.verifyJwtToken, ctrlUser.userProfile);
router.get('/player-count', ctrlServer.getNbPlayer);
router.get('/is-online', ctrlServer.isOnline);

module.exports = router;
